function add(a, b) {
    return a + b;
}

function httpGet(url) {
    
    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", url, false ); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

token.setOtherClaims("my_username", user.username);
token.setOtherClaims("my_sum", add(3, 4));
token.setOtherClaims("my_get", httpGet("https://jsonplaceholder.typicode.com/posts"));
