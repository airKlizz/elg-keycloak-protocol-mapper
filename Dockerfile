FROM openjdk:8-jdk-alpine

ENV TARGETDIR /elg/
RUN mkdir -p $TARGETDIR
ADD target/keycloak-customProtocolMapper-1.0-SNAPSHOT.jar ${TARGETDIR}/mapper.jar
RUN ls ${TARGETDIR}
WORKDIR ${TARGETDIR}